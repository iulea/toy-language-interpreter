package View;

import Controller.Ctrl;
import Model.Adt.*;
import Model.Command.ExitCommand;
import Model.Command.RunExample;
import Model.Expression.*;
import Model.ProgramState;
import Model.Statement.*;
import Repository.IRepo;
import Repository.Repo;
import javafx.util.Pair;

import java.io.BufferedReader;
import java.util.Scanner;

public class View {

//    private static void menu(){
//        System.out.println("Select an existing program: ");
//        System.out.println("1. v = 2;Print(v) ");
//        System.out.println("2. a = 2 + 3 * 5;b = a + 1;Print(b)");
//        System.out.println("3. a = 2 - 2;(If a Then v = 2 Else v = 3);Print(v)");
//        System.out.println("4. openRFile(var_f,\"test.in\"); \n" +
//                "readFile(var_f,var_c);print(var_c); \n" +
//                "(if var_c then readFile(var_f,var_c);print(var_c) \n" +
//                "else print(0)); \n" +
//                "closeRFile(var_f) ");
//        System.out.println("5. openRFile(var_f,\"test.in\"); \n" +
//                "readFile(var_f+2,var_c);print(var_c); \n" +
//                "(if var_c then readFile(var_f,var_c);print(var_c) \n" +
//                "else print(0)); \n" +
//                "closeRFile(var_f) ");
//    }

    public static void main(String args[]){


//        IStatement stm1 = new CompStmt(new AssignStmt("v",new ConstExp(2)), new PrintStmt(new VarExp("v")));
//
//        MyDictionary<String, Integer> symTable1 = new MyDictionary<>();
//        MyList<Integer> output1 = new MyList<>();
//        MyStack<IStatement> exeStack1 = new MyStack<>();
//        MyDictionary<Integer, Pair<String, BufferedReader>> fileTable1 = new MyDictionary<>();
//
//        ProgramState prg1 = new ProgramState(symTable1, exeStack1, output1, stm1, fileTable1);
//        Repo repo1 = new Repo("fisier1.txt");
//        repo1.add(prg1);
//        Ctrl ctrl1 = new Ctrl(repo1);
//
//        IStatement stm2 =  new CompStmt(new AssignStmt("a", new ArithExp(new ConstExp(2),new ArithExp(new ConstExp(3), new ConstExp(5), '*'), '+')),
//                    new CompStmt(new AssignStmt("b",new ArithExp(new VarExp("a"), new ConstExp(1), '+')), new PrintStmt(new VarExp("b"))));
//
//        MyDictionary<String, Integer> symTable2 = new MyDictionary<>();
//        MyList<Integer> output2 = new MyList<>();
//        MyStack<IStatement> exeStack2 = new MyStack<>();
//        MyDictionary<Integer, Pair<String, BufferedReader>> fileTable2 = new MyDictionary<>();
//
//        ProgramState prg2 = new ProgramState(symTable2, exeStack2, output2, stm2, fileTable2);
//        Repo repo2 = new Repo("fisier2.txt");
//        repo2.add(prg2);
//        Ctrl ctrl2 = new Ctrl(repo2);
//
//        IStatement stm3 = new CompStmt(new AssignStmt("a", new ArithExp(new ConstExp(2), new ConstExp(2), '-')),
//                    new CompStmt(new IfStmt(new VarExp("a"),new AssignStmt("v",new ConstExp(2)), new AssignStmt("v", new ConstExp(3))),
//                            new PrintStmt(new VarExp("v"))));
//
//        MyDictionary<String, Integer> symTable3 = new MyDictionary<>();
//        MyList<Integer> output3 = new MyList<>();
//        MyStack<IStatement> exeStack3 = new MyStack<>();
//        MyDictionary<Integer, Pair<String, BufferedReader>> fileTable3 = new MyDictionary<>();
//
//        ProgramState prg3 = new ProgramState(symTable3, exeStack3, output3, stm3, fileTable3);
//        Repo repo3 = new Repo("fisier3.txt");
//        repo3.add(prg3);
//        Ctrl ctrl3 = new Ctrl(repo3);
//
//        IStatement stm4 = new CompStmt(new OpenRFile("test.in.txt", "var_f"), new CompStmt(new ReadFile(new VarExp("var_f"), "var_c"),
//                        new CompStmt(new PrintStmt(new VarExp("var_c")), new CompStmt(new IfStmt(new VarExp("var_c"), new CompStmt(
//                                new ReadFile(new VarExp("var_f"), "var_c"), new PrintStmt(new VarExp("var_c"))), new PrintStmt(new ConstExp(0))),
//                                        new CloseRFile(new VarExp("var_f"))))));
//
//        MyDictionary<String, Integer> symTable4 = new MyDictionary<>();
//        MyList<Integer> output4 = new MyList<>();
//        MyStack<IStatement> exeStack4 = new MyStack<>();
//        MyDictionary<Integer, Pair<String, BufferedReader>> fileTable4 = new MyDictionary<>();
//
//        ProgramState prg4 = new ProgramState(symTable4, exeStack4, output4, stm4, fileTable4);
//        Repo repo4 = new Repo("fisier4.txt");
//        repo4.add(prg4);
//        Ctrl ctrl4 = new Ctrl(repo4);
//
//        IStatement prg5 = new CompStmt(new OpenRFile("test.in.txt", "var_f"), new CompStmt(new ReadFile(new ArithExp(new VarExp(
//                "var_f"), new ConstExp( 2), '+'), "var_c"), new CompStmt(new PrintStmt(new VarExp("var_c")), new CompStmt(new IfStmt(
//                        new VarExp("var_c"), new CompStmt(new ReadFile(new VarExp("var_f"), "var_c"), new PrintStmt(new VarExp("var_c"))),
//                            new PrintStmt(new ConstExp(0))), new CloseRFile(new VarExp("var_f"))))));
//
//        MyDictionary<String, Integer> symTable5 = new MyDictionary<>();
//        MyList<Integer> output5 = new MyList<>();
//        MyStack<IStatement> exeStack5 = new MyStack<>();
//        MyDictionary<Integer, Pair<String, BufferedReader>> fileTable5 = new MyDictionary<>();
//
//        ProgramState programState5 = new ProgramState(symTable5, exeStack5, output5, prg5, fileTable5);
//        Repo repo5 = new Repo("fisier5.txt");
//        repo5.add(programState5);
//        Ctrl ctrl5 = new Ctrl(repo5);


        MyDictionary<String, Integer> symTable6 = new MyDictionary<>();
        MyList<Integer> output6 = new MyList<>();
        MyStack<IStatement> exeStack6 = new MyStack<>();
        MyDictionary<Integer, Pair<String, BufferedReader>> fileTable6 = new MyDictionary<>();
        MyHeap<Integer, Integer> heap6 = new MyHeap<>();
        IStatement ex6 = new CompStmt(
                new AssignStmt("v", new ConstExp(10)), new CompStmt(new AllocStmt("v", new ConstExp(20)), new CompStmt(new AllocStmt("a", new ConstExp(22)),
                    new PrintStmt(new VarExp("v"))
                        )
                )
        );
        ProgramState prg6 = new ProgramState(symTable6, exeStack6, output6, ex6,fileTable6, heap6, 1);
        Repo repo6 = new Repo("test6.txt");
        repo6.add(prg6);
        Ctrl ctrl6 = new Ctrl(repo6);

        //v=6; (while (v-4) print(v);v=v-1);print(v)
        MyDictionary<String, Integer> symTable9 = new MyDictionary<>();
        MyList<Integer> output9 = new MyList<>();
        MyStack<IStatement> exeStack9 = new MyStack<>();
        MyDictionary<Integer, Pair<String, BufferedReader>> fileTable9 = new MyDictionary<>();
        MyHeap<Integer, Integer> heap9 = new MyHeap<>();
        IStatement ex9 = new CompStmt(
                new AssignStmt("v", new ConstExp(6)),
                new CompStmt(
                        new WhileStmt(new ArithExp(new VarExp("v"), new ConstExp(4), '-'),
                                new CompStmt(
                                        new PrintStmt(new VarExp("v")),
                                        new AssignStmt("v", new ArithExp(new VarExp("v"), new ConstExp(1), '-'))
                                )),
                        new PrintStmt(new VarExp("v")))
        );
        ProgramState programState9 = new ProgramState(symTable9, exeStack9, output9, ex9,fileTable9, heap9, 1);
        Repo repo9 = new Repo("text9.txt");
        repo9.add(programState9);
        Ctrl ctrl9 = new Ctrl(repo9);



        MyDictionary<String, Integer> symTable1 = new MyDictionary<>();
        MyList<Integer> output1 = new MyList<>();
        MyStack<IStatement> exeStack1 = new MyStack<>();
        MyDictionary<Integer, Pair<String, BufferedReader>> fileTable1 = new MyDictionary<>();
        MyHeap<Integer, Integer> heap1 = new MyHeap<>();
        IStatement ex1 = new CompStmt(new AssignStmt("v", new ConstExp(10)), new CompStmt(new AllocStmt("a", new ConstExp(22)), new CompStmt(
                new ForkSTmt(new CompStmt(new HeapWritingStmt("a", new ConstExp(30)),new CompStmt(new AssignStmt("v", new ConstExp(32)), new CompStmt(new PrintStmt(new VarExp("v")), new PrintStmt(new HeapReadingExp("a")))))),
                new CompStmt(new PrintStmt(new VarExp("v")), new PrintStmt(new HeapReadingExp("a")))
        )));
        ProgramState programState1 = new ProgramState(symTable1, exeStack1, output1, ex1,fileTable1, heap1, 1);
        Repo repo1 = new Repo("text1.txt");
        repo1.add(programState1);
        Ctrl ctrl1 = new Ctrl(repo1);

        TextMenu menu = new TextMenu();
        menu.addCommand(new ExitCommand("0", "exit"));
        menu.addCommand(new RunExample("1", programState1.toString(), ctrl1));
//        menu.addCommand(new RunExample("2", prg2.toString(), ctrl2));
//        menu.addCommand(new RunExample("3", prg3.toString(), ctrl3));
//        menu.addCommand(new RunExample("4", prg4.toString(), ctrl4));
//        menu.addCommand(new RunExample("5", prg5.toString(), ctrl5));
        menu.addCommand(new RunExample("6", prg6.toString(), ctrl6));
        menu.addCommand(new RunExample("9", programState9.toString(), ctrl9));
        menu.show();


    }
}
