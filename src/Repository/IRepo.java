package Repository;

import Model.Adt.MyList;
import Model.ProgramState;

import java.util.List;

public interface IRepo {
    void add(ProgramState p);
    List<ProgramState> getPrgList();
    void setPrgList(List<ProgramState> lista);
}