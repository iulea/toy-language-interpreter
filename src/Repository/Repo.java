package Repository;

import Model.Adt.MyList;
import Model.ProgramState;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class Repo implements IRepo{

    private List<ProgramState> prgStates;
    private int current;
    private String logFile;
    private PrintWriter printWriter;

    public Repo(String logFile){
        prgStates = new ArrayList<ProgramState>();
        current = 0;
        this.logFile = logFile;
    }

    @Override
    public void add(ProgramState p) {
        this.prgStates.add(p);
    }

    public List<ProgramState> getPrgList(){
        return this.prgStates;
    }

    public void setPrgList(List<ProgramState> lista){
        this.prgStates = lista;
    }

    public void logPrgStateExec(ProgramState crt) throws IOException {

        printWriter = new PrintWriter(new BufferedWriter(new FileWriter(logFile, true)));
        printWriter.println("Id: " + crt.getId());
        printWriter.println("ExeStack: ");
        printWriter.println(crt.getExeStack().toString());
        printWriter.println("SymTable:");
        printWriter.println(crt.getSymTable().toString());
        printWriter.println("Output");
        printWriter.println(crt.getOutput().toString());
        printWriter.println("Heap");
        printWriter.println(crt.getHeap().toString());
        printWriter.println(".........................");
        printWriter.close();
    }
}
