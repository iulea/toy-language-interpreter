package Controller;

import Model.Adt.MyDictionary;
import Model.Adt.MyHeap;
import Model.Adt.MyIStack;
import Model.Adt.MyStack;
import Model.Expression.VarExp;
import Model.ProgramState;
import Model.Statement.CloseRFile;
import Model.Statement.IStatement;
import Repository.IRepo;
import Exceptions.ADTException;
import Repository.Repo;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.zip.Adler32;

public class Ctrl {

    private Repo repo;
    private boolean flag;
    private ExecutorService executor;

    public Ctrl(Repo repo){

        this.repo = repo;
        this.flag = true;
    }

    private Map<Integer,Integer> conservativeGarbageCollector(Collection<Integer> symTableValues, Map<Integer,Integer> heap){
        return heap.entrySet().stream().filter(e->symTableValues.contains(e.getKey()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }


    List<ProgramState> removeCompletedPrg(List<ProgramState> inPrgList){
        return inPrgList.stream()
                .filter(ProgramState::isNotCompleted)
                .collect(Collectors.toList());
    }

    public void closeOpenedFiles(ProgramState ps) throws IOException {

        Collection<Integer> fileTableKeys = ps.getFileTable().getKeys();
        MyDictionary<String, Integer> symTable = ps.getSymTable();
        List<Map.Entry<String, Integer>> keys = symTable.entrySet().stream().filter(e->fileTableKeys.contains(e.getValue())).collect(Collectors.toList());
        for(Map.Entry<String, Integer> e : keys) {
            if (ps.getFileTable().containsKey(e.getValue()))
                new CloseRFile(new VarExp(e.getKey())).execute(ps);
        }
    }


    public void oneStep(List<ProgramState> prgList) throws IOException, InterruptedException {
        prgList.forEach(prg -> {
            try {
                repo.logPrgStateExec(prg);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        List<Callable<ProgramState>> callList = prgList.stream()
                .map((ProgramState p) -> (Callable<ProgramState>) (p::oneStep))
                .collect(Collectors.toList());

        List<ProgramState> newPrgList = executor.invokeAll(callList).stream().map(future -> {
            try {
                return future.get();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }).filter(Objects::nonNull).collect(Collectors.toList());

        prgList.addAll(newPrgList);
        prgList.forEach(prg -> {
            try {
                repo.logPrgStateExec(prg);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        repo.setPrgList(prgList);
    }

    public void allStep() throws IOException, InterruptedException {
        executor = Executors.newFixedThreadPool(2);
        //remove the completed programs
        List<ProgramState>  prgList=repo.getPrgList();
        if(repo.getPrgList()!=null)
            prgList=removeCompletedPrg(repo.getPrgList());

        ProgramState firstPS = prgList.get(0);
        MyHeap<Integer, Integer> heap = prgList.get(0).getHeap();
        Collection<Integer> fileTable = prgList.get(0).getFileTable().getKeys();
        while(prgList.size() > 0){
            prgList.forEach(p -> {conservativeGarbageCollector(p.getSymTable().values(), heap.getContent());});
            oneStep(prgList);
            //remove the completed programs
            prgList=removeCompletedPrg(repo.getPrgList());
        }
        executor.shutdownNow();
        //HERE the repository still contains at least one Completed Prg
        // and its List<PrgState> is not empty. Note that oneStepForAllPrg calls the method
        //setPrgList of repository in order to change the repository
        // update the repository state
        repo.setPrgList(prgList);

        closeOpenedFiles(firstPS);
    }

}
