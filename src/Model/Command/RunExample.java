package Model.Command;

import Controller.Ctrl;
import Exceptions.ADTException;

import javax.imageio.IIOException;
import java.io.IOException;

public class RunExample extends Command {

    private Ctrl ctr;
    public RunExample(String key, String desc,Ctrl ctr){
        super(key, desc);
        this.ctr=ctr;
    }
    @Override
    public void execute(){
        try{
            ctr.allStep();  }
        catch (IOException e)  {
            System.out.println(e.toString());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
