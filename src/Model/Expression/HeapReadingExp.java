package Model.Expression;

import Model.Adt.MyDictionary;
import Model.Adt.MyHeap;
import Model.Adt.MyIDictionary;
import Model.Adt.MyIHeap;

public class HeapReadingExp implements IExpression {
    private String varName;

    public HeapReadingExp(String varName){
        this.varName = varName;
    }

    public int evaluate(MyDictionary<String, Integer> symTable, MyHeap<Integer, Integer> heap){
        int index = symTable.getValue(varName); //heap address
        return heap.find(index);
    }

    @Override
    public String toString(){
        String s = "";
        s += "readHeap(" + varName + ")";
        return s;
    }
}
