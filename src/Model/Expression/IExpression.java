package Model.Expression;

import Model.Adt.MyDictionary;
import Model.Adt.MyHeap;


public interface IExpression {
    int evaluate(MyDictionary<String, Integer> st, MyHeap<Integer, Integer> heap);
}
