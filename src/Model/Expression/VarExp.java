package Model.Expression;

import Model.Adt.MyDictionary;
import Model.Adt.MyHeap;

public class VarExp implements IExpression {
    private String name;

    public VarExp(String name){ this.name = name; }

    public int evaluate(MyDictionary<String, Integer> st, MyHeap<Integer, Integer> heap)
    {
        if(st.find(this.name))
        {
            return st.getValue(this.name);
        }
        else
        {
            throw new RuntimeException("Inexistent variable");
        }
    }


    public String toString() {
        return this.name;
    }
}
