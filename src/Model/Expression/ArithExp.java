package Model.Expression;

import Model.Adt.MyDictionary;
import Model.Adt.MyHeap;

public class ArithExp implements IExpression{
    private IExpression op1, op2;
    private char operator;

    public ArithExp(IExpression op1, IExpression op2, char operator){
        this.op1 = op1;
        this.op2 = op2;
        this.operator = operator;
    }

    public int evaluate(MyDictionary<String, Integer> st, MyHeap<Integer, Integer> heap)
    {
        int firstRes = this.op1.evaluate(st, heap);
        int secondRes = this.op2.evaluate(st, heap);

        switch (this.operator)
        {
            case '+':
                return firstRes + secondRes;
            case '-':
                return firstRes - secondRes;
            case '*':
                return firstRes * secondRes;
            case '/':
                if(secondRes == 0)
                {
                    //throw exception
                }
                return firstRes / secondRes;
            default:
                throw new RuntimeException("invalid");
        }
    }

    public String toString() {
        return op1.toString() + operator + op2.toString();
    }
}
