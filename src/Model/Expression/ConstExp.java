package Model.Expression;

import Model.Adt.MyDictionary;
import Model.Adt.MyHeap;

public class ConstExp implements IExpression {
    private int value;

    public ConstExp(int nr){
        this.value = nr;
    }

    public int evaluate(MyDictionary<String, Integer> st, MyHeap<Integer, Integer> heap) {
        return this.value;
    }

    public String toString() {
        return String.valueOf(this.value);
    }
}
