package Model.Expression;

import Exceptions.ADTException;
import Model.Adt.MyDictionary;
import Model.Adt.MyHeap;

public class BooleanExp implements IExpression {
    private IExpression exp1;
    private IExpression exp2;
    private String type;

    public BooleanExp(IExpression exp1, IExpression exp2, String type){
        this.exp1 = exp1;
        this.exp2 = exp2;
        this.type = type;
    }

    public int evaluate(MyDictionary<String, Integer> symTable, MyHeap<Integer, Integer> heap) {
        int value1 = this.exp1.evaluate(symTable, heap);
        int value2 = this.exp2.evaluate(symTable, heap);

        switch(type){
            case "==":
                return value1 == value2 ? 1 : 0;
            case "<":
                return value1 < value2 ? 1 : 0;
            case "<=":
                return value1 <= value2 ? 1 : 0;
            case ">":
                return value1 > value2 ? 1 : 0;
            case ">=":
                return value1 >= value2 ? 1 : 0;
            case "!=":
                return value1 != value2 ? 1 : 0;
            default:
                throw new ADTException("Invalid type of comparison.");
        }
    }

    @Override
    public String toString(){
        return "( " + exp1.toString() + " " + this.type + " " + exp2.toString() + " )";
    }
}
