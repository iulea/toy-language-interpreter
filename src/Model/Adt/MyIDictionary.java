package Model.Adt;

public interface MyIDictionary<k,v>{

    void add(k name, v val);
    boolean find(k name);
    v getValue(k name);

}
