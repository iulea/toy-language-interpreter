package Model.Adt;

import Exceptions.ADTException;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class MyHeap<T1, T2> implements MyIHeap<T1, T2> {
    private Map<T1, T2> heap;
    private Integer freeLocation;

    public MyHeap(){
        this.heap = new HashMap<>();
        this.freeLocation = 1;
    }

    public void allocate(T1 addr, T2 t){
        this.heap.put(addr, t);
        setNewFreeLocation();
    }

    public void setNewFreeLocation(){
        this.freeLocation+=1;
    }

    public String toString(){
        String s = "";
        for(T1 key : heap.keySet()){

            s += key.toString();
            s += "->";
            s += heap.get(key).toString();
            s +=  "\n";
        }

        return s;
    }

    @Override
    public T2 find(T1 key) throws ADTException {
        T2 el=heap.get(key);
        if(el!=null)
            return el;
        throw new ADTException("The variable "+key.toString()+" is not defined!");
    }

    public void remove(T1 key) throws ADTException {
        try{
            find(key);
        }catch (ADTException e){
            throw new ADTException(e.getMessage()+"Nothing to remove!");
        }
        heap.remove(key);
    }

    @Override
    public boolean isDefined(T1 id) {
        return this.heap.containsKey(id);
    }

    @Override
    public void update(T1 id, T2 val) throws ADTException {
        try{
            find(id);
        }catch (ADTException e)
        {
            throw new ADTException(e.getMessage()+" Nothing to update!");
        }
        this.heap.put(id,val);
    }

    @Override
    public void setContent(Map<T1,T2> m) {
        this.heap = m;
    }

    @Override
    public Map<T1, T2> getContent() {
        return this.heap;
    }

    @Override
    public int getNewFreeLocation() {
        return this.freeLocation;
    }

}
