package Model.Adt;


public interface MyIStack<T>{
    void push(T number);
    T pop();
    boolean isEmpty();
}