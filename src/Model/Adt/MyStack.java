package Model.Adt;


import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Stack;

public class MyStack<T> implements MyIStack<T> {

    private Stack<T> stack;

    public MyStack() {
        this.stack = new Stack<T>();
    }

    public boolean isEmpty() {
        return stack.isEmpty();
    }

    public void push(T element) {
        stack.push(element);

    }

    public T pop() {
        return stack.pop();
    }

    public String toString(){
        StringBuilder s = new StringBuilder();
        for(T elem : stack)
            s.append(elem.toString()).append(" ");
        return s.toString();
    }
}
