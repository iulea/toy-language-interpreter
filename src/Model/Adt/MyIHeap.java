package Model.Adt;

import Exceptions.ADTException;

import java.util.Map;

public interface MyIHeap<T1, T2> {
    void allocate(T1 addr, T2 t);
    String toString();
    void update(T1 addr, T2 t);
    void setContent(Map<T1, T2> m);
    Map<T1, T2> getContent();
    T2 find(T1 key) throws ADTException;
    boolean isDefined(T1 id);
    int getNewFreeLocation();
    void setNewFreeLocation();
}