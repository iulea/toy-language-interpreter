package Model.Adt;

import java.util.ArrayList;

public class MyList<T> implements MyIList<T> {

    private ArrayList<T> elems;

    public MyList(){
        this.elems = new ArrayList<T>(100);
    }

    public void add(T elem) {
        this.elems.add(elem);
    }

    public T get(int index){
        return elems.get(index);
    }

    public String toString(){
        String res = "";
        for(T elem : this.elems)
            res += elem.toString() + " ";
        return res;
    }


}
