package Model.Adt;

import Exceptions.ADTException;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


public class MyDictionary<k,v> implements MyIDictionary<k,v> {
    private Map<k, v> map;

    public MyDictionary(){
        this.map = new HashMap<k,v>();
    }

    public MyDictionary(MyDictionary<k, v> dict){ this.map = dict.map; }

    public void add(k name, v value){
        map.put(name, value);
    }

    public boolean find(k name) {
        return (map.get(name)!=null);
    }

    public v getValue(k name){
        return map.get(name);
    }

    public String toString(){
        StringBuilder res = new StringBuilder();
        for(k key : map.keySet())
            res.append(key.toString()).append(" -> ").append(map.get(key)).append("\n");
        return res.toString();
    }

    public Collection<v> values(){
        return map.values();
    }

    public v get(k key) {
        if(map.get(key) == null)
            throw new ADTException("Invalid key");
        return map.get(key);
    }

    public Collection<k> getKeys(){
        return this.map.keySet();
    }

    public Set<Map.Entry<k,v>> entrySet(){
        return this.map.entrySet();
    }

    public Boolean containsKey(k key){
        return this.map.containsKey(key);
    }

    public void remove(k key){
        map.remove(key);
    }
}
