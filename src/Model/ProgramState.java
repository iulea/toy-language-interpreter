package Model;

import Exceptions.ADTException;
import Model.Adt.*;
import Model.Statement.IStatement;
import javafx.util.Pair;

import java.io.BufferedReader;

public class ProgramState {
    private IStatement program;

    private MyDictionary<String, Integer> symTable;
    private MyStack<IStatement> exeStack;
    private MyList<Integer> output;
    private MyDictionary<Integer, Pair<String, BufferedReader>> fileTable;
    private MyHeap<Integer, Integer> heap;
    private int id;

    public ProgramState(MyDictionary<String, Integer> symTable, MyStack<IStatement> exeStack, MyList<Integer> output, IStatement program, MyDictionary<Integer, Pair<String, BufferedReader>> fileTable, MyHeap<Integer, Integer> heap, int id){
        this.symTable = symTable;
        this.exeStack = exeStack;
        this.output = output;
        this.fileTable = fileTable;
        this.heap = heap;
        this.exeStack.push(program);
        this.id = id;
    }

    public MyDictionary<String, Integer> getSymTable()
    {
        return symTable;
    }

    public void setSymTable(MyDictionary symTable)
    {
        this.symTable = symTable;
    }

    public MyStack<IStatement>
    getExeStack()
    {
        return exeStack;
    }

    public void setExeStack(MyStack exeStack)
    {
        this.exeStack = exeStack;
    }

    public MyList<Integer> getOutput()
    {
        return output;
    }

    public void setOutput(MyList output)
    {
        this.output = output;
    }

    public void setProgram(IStatement prg)
    {
        this.program = prg;
    }

    public IStatement getProgram()
    {
        return this.program;
    }

    public MyDictionary<Integer, Pair<String, BufferedReader>> getFileTable() {
        return fileTable;
    }

    public void setFileTable(MyDictionary<Integer, Pair<String, BufferedReader>> file){ this.fileTable = file; }

    public MyHeap<Integer, Integer> getHeap(){ return this.heap; }

    public void setHeap(MyHeap<Integer, Integer> heap) { this.heap = heap; }

    public int getId(){ return this.id; }

    public void setId(int id) { this.id = id; }

    public Boolean isNotCompleted(){
        return !(this.exeStack.isEmpty());
    }

    public ProgramState oneStep() throws Exception {
        if(exeStack.isEmpty())
            throw new Exception(" ");
        IStatement crtStmt = exeStack.pop();
        return crtStmt.execute(this);
    }

    public String toString() {
        String s = "";
        s += "Id: " + this.id + "\n";
        s += "exeStack:\n";
        s += this.exeStack.toString();
        s += "\nsymTable:";
        s += this.symTable.toString();
        s += "\noutput:";
        s += this.output.toString();
        return s;
    }
}
