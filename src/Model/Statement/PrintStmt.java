package Model.Statement;

import Model.Adt.MyHeap;
import Model.Adt.MyList;
import Model.Expression.IExpression;
import Model.ProgramState;

import java.io.IOException;

public class PrintStmt implements IStatement {
    private IExpression exp;

    public PrintStmt(IExpression exp){
        this.exp = exp;
    }

    public ProgramState execute(ProgramState ps) throws IOException {
        MyList<Integer> output = ps.getOutput();
        output.add(exp.evaluate(ps.getSymTable(), ps.getHeap()));
        return null;
    }

    @Override
    public String toString() {
        return "Print (" + exp.toString() + ")";
    }
}
