package Model.Statement;

import Model.Adt.MyStack;
import Model.ProgramState;

import java.io.IOException;

public class CompStmt implements IStatement {
    private IStatement st1, st2;

    public CompStmt(IStatement st1, IStatement st2){
        this.st1 = st1;
        this.st2 = st2;
    }

    public ProgramState execute(ProgramState ps) throws IOException {
        

        MyStack<IStatement> exeStack = ps.getExeStack();
        exeStack.push(this.st2);
        exeStack.push(this.st1);

        return null;
    }

    @Override
    public String toString() {
        return this.st1.toString() + " ; " + this.st2.toString() + " ; ";
    }
}
