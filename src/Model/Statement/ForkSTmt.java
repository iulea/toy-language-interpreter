package Model.Statement;

import Model.Adt.MyDictionary;
import Model.Adt.MyHeap;
import Model.Adt.MyList;
import Model.Adt.MyStack;
import Model.ProgramState;
import javafx.util.Pair;

import java.io.BufferedReader;

public class ForkSTmt implements IStatement {

    private IStatement stmt;

    public ForkSTmt(IStatement stmt){

        this.stmt = stmt;
    }

    @Override
    public ProgramState execute(ProgramState ps) {
        MyStack<IStatement> exeStack = new MyStack<>();
        MyDictionary<String, Integer> symTable = new MyDictionary<>(ps.getSymTable());
        MyHeap<Integer, Integer> heap = ps.getHeap();
        MyDictionary<Integer, Pair<String, BufferedReader>> fileTable = ps.getFileTable();
        MyList<Integer> output = ps.getOutput();
        int id = ps.getId() * 10;
        ProgramState prg = new ProgramState(symTable,exeStack, output, stmt, fileTable, heap, id);

        return prg;
    }

    @Override
    public String toString() {
        return "forkStatement(" +  stmt.toString() + ")";
    }
}
