package Model.Statement;

import Model.Adt.MyDictionary;
import Model.Adt.MyHeap;
import Model.Adt.MyIDictionary;
import Model.Adt.MyIHeap;
import Model.Expression.IExpression;
import Model.ProgramState;

import java.io.IOException;

public class AllocStmt implements IStatement {
    private String var;
    private IExpression expr;

    public AllocStmt(String var, IExpression expr){
        this.var = var;
        this.expr = expr;
    }
    @Override
    public ProgramState execute(ProgramState programState) throws IOException {
        MyDictionary<String, Integer> symTable = programState.getSymTable();
        MyHeap<Integer, Integer> heap = programState.getHeap();
        int value = this.expr.evaluate(symTable, heap);

        programState.getSymTable().add(var,programState.getHeap().getNewFreeLocation());
        programState.getHeap().allocate(programState.getHeap().getNewFreeLocation(), value);

        return null;
    }

    public String toString(){
        String s = "";
        s += "new ( " + var + "," + expr.toString() + ")";
        return s;
    }
}
