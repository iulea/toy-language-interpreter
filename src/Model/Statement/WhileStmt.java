package Model.Statement;

import Model.Adt.MyStack;
import Model.Expression.IExpression;
import Model.ProgramState;

public class WhileStmt implements IStatement {
    private IExpression exp;
    private IStatement doStatement;

    public WhileStmt(IExpression exp, IStatement doStatement){
        this.exp = exp;
        this.doStatement = doStatement;
    }
    public ProgramState execute(ProgramState ps) {
        MyStack<IStatement> exeStack = ps.getExeStack();

        if(this.exp.evaluate(ps.getSymTable(), ps.getHeap()) != 0){
            exeStack.push(this);
            exeStack.push(this.doStatement);
        }

        return null;
    }

    @Override
    public String toString(){
        return "while ( " + this.exp.toString() + " ) do { " + this.doStatement.toString() + " } ";
    }
}