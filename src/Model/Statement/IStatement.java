package Model.Statement;

import Model.ProgramState;

import java.io.IOException;

public interface IStatement {
    ProgramState execute(ProgramState ps) throws IOException;

}
