package Model.Statement;

import Model.Adt.MyDictionary;
import Model.Adt.MyHeap;
import Model.Expression.IExpression;
import Model.ProgramState;

import java.io.IOException;

public class AssignStmt implements IStatement {
    private String varName;
    private IExpression expr;

    public AssignStmt(String varName, IExpression expr){
        this.expr = expr;
        this.varName = varName;
    }

    public ProgramState execute(ProgramState ps) throws IOException {
        MyDictionary<String, Integer> st = ps.getSymTable();
        MyHeap<Integer, Integer> heap = ps.getHeap();
        st.add(this.varName, this.expr.evaluate(st, heap));
        return null;
    }

    public String toString() {
        return this.varName.toString() + " = " + this.expr.toString();
    }
}
