package Model.Statement;

import Exceptions.ADTException;
import Model.Adt.MyDictionary;
import Model.Adt.MyHeap;
import Model.Adt.MyStack;
import Model.Expression.IExpression;
import Model.ProgramState;
import javafx.util.Pair;

import java.io.BufferedReader;
import java.io.IOException;

public class CloseRFile implements IStatement {

    private IExpression expressionFileId;

    public CloseRFile(IExpression expressionFileId) {
        this.expressionFileId = expressionFileId;
    }

    @Override
    public ProgramState execute(ProgramState programState) throws IOException {
        MyStack<IStatement> exeStack = programState.getExeStack();
        MyDictionary<String, Integer> symTable = programState.getSymTable();
        MyDictionary<Integer, Pair<String, BufferedReader>> fileTable = programState.getFileTable();
        MyHeap<Integer, Integer> heap = programState.getHeap();
        int val = expressionFileId.evaluate(symTable, heap);
        BufferedReader reader = fileTable.getValue(val).getValue();
        if( reader == null ){
            throw new ADTException("No such file descriptor");
        }
        reader.close();
        fileTable.remove(val);
        return null;
    }

    @Override
    public String toString() {
        return "Close the file.";
    }
}
