package Model.Statement;

import Exceptions.ADTException;
import Model.Adt.MyDictionary;
import Model.Adt.MyStack;
import Model.ProgramState;
import javafx.util.Pair;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class OpenRFile implements IStatement {

    private static int numberOfVarFileId = 1;
    private String filename;
    private String varFileId;

    public OpenRFile(String filename, String varFileId) {
        this.filename = filename;
        this.varFileId = varFileId;
    }

    @Override
    public ProgramState execute(ProgramState programState) throws ADTException, FileNotFoundException {
        MyStack<IStatement> exeStack = programState.getExeStack();
        MyDictionary<String, Integer> symTable = programState.getSymTable();
        MyDictionary<Integer, Pair<String, BufferedReader>> fileTable = programState.getFileTable();
        for(Pair<String, BufferedReader> e : fileTable.values())
            if(e.getKey().equals(filename))
                throw new ADTException("Filename already exists");

        BufferedReader reader = new BufferedReader(new FileReader(filename));
        Pair<String, BufferedReader> p = new Pair<>(filename, reader);
        fileTable.add(numberOfVarFileId, p);
        symTable.add(varFileId, numberOfVarFileId);
        numberOfVarFileId ++;

        return null;
    }

    @Override
    public String toString() {
        return "Open file: " + filename;
    }
}
