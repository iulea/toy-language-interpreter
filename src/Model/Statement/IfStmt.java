package Model.Statement;

import Model.Expression.IExpression;
import Model.ProgramState;

import java.io.IOException;

public class IfStmt implements IStatement {

    private IExpression exp;
    private IStatement st1, st2;

    public IfStmt( IExpression exp, IStatement st1, IStatement st2){
        this.exp = exp;
        this.st1 = st1;
        this.st2 = st2;
    }

    public ProgramState execute(ProgramState ps) throws IOException {

        int rez = this.exp.evaluate(ps.getSymTable(), ps.getHeap());
        if(rez != 0)
            this.st1.execute(ps);
        else
            this.st2.execute(ps);
        return null;
    }

    @Override
    public String toString() {
        return "if " + this.exp.toString() + " then " + this.st1.toString() + " else " + this.st2.toString();
    }
}
