package Model.Statement;

import Model.Adt.MyDictionary;
import Model.Adt.MyHeap;
import Model.Expression.IExpression;
import Model.ProgramState;

import java.io.IOException;

public class HeapWritingStmt implements IStatement {
    private String varName;
    private IExpression expression;

    public HeapWritingStmt(String varName, IExpression expression) {
        this.varName = varName;
        this.expression = expression;
    }

    @Override
    public ProgramState execute(ProgramState ps) throws IOException {
        MyDictionary<String, Integer> symTable = ps.getSymTable();
        MyHeap<Integer, Integer> heap = ps.getHeap();
        int address = symTable.getValue(varName);
        int value  = expression.evaluate(symTable, heap);
        heap.update(address, value);
        return null;
    }

    @Override
    public String toString(){
        String s = "";
        s += "heapWrite ( " + varName + ", " + expression.toString() + " ) ";
        return s;
    }

}
